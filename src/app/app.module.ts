import { MatIconRegistry, MatIconModule, MatToolbarModule } from '@angular/material';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';

import { routing } from './app.routing';

import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { JwtInterceptor, ApiInterceptor } from './_helpers/index';
import { AlertService, AuthenticationService, TimesheetService } from './_services/index';

import { LayoutComponent } from './_layout/layout.component';
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { TimesheetsDialogComponent } from './home/timesheetsDialog/timesheetsDialog.component';

import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminComponent } from './admin/admin.component';
import { DndModule } from 'ng2-dnd';
import { NewTicketComponent } from './dashboard/newTicket/newTicket.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    LayoutComponent,
    HomeComponent,
    LoginComponent,
    TimesheetsDialogComponent,
    DashboardComponent,
    AdminComponent,
    NewTicketComponent
  ],
  entryComponents: [TimesheetsDialogComponent, NewTicketComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatToolbarModule,
    routing,
    DndModule.forRoot()
  ],
  providers: [
    AuthGuard,
    AlertService,
    AuthenticationService,
    TimesheetService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
    matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg'));
  }
}