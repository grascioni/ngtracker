import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add API key header
        request = request.clone({
            setHeaders: {
                API_KEY: 'OVi3nM6fASu1PnI5pNoF1AFCKLJcftxhAyI6mWASgb8%3d'
            }
        });

        return next.handle(request);
    }
}