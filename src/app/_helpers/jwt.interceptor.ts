import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.Id) {
            //HACK: Since we don't control the API and it doesn't return a token on the login response, we use the user's Id instead.
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentUser.Id}`
                }
            });
        }

        return next.handle(request);
    }
}