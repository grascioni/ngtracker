import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from './_layout/layout.component';
import { HomeComponent } from './home/index';
import { DashboardComponent } from './dashboard/index';
import { AdminComponent } from './admin/index';
import { LoginComponent } from './login/index';
import { AuthGuard } from './_guards/index';

const appRoutes: Routes = [
    { 
        path: '', 
        component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
          { path: '', component: HomeComponent, pathMatch: 'full'},
          { path: 'dashboard/:projectId', component: DashboardComponent },
          { path: 'admin', component: AdminComponent }
        ]
    },

    { path: 'login', component: LoginComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);