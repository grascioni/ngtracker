import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee, Timesheet, Project, Ticket } from '../_models/index';
import * as moment from 'moment';

@Injectable()
export class TimesheetService {
    currentUser: Employee;

    constructor(private http: HttpClient) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    private apiBaseUrl = 'http://aac-vm.universe.dart.spb:8080/api';

    getAll(dateFrom: Date, dateTo: Date) {
        const dateFromFormatted = moment(dateFrom).format('MM-DD-YYYY');
        const dateToFormatted = moment(dateTo).format('MM-DD-YYYY');
        const queryString = `?query.empId=${this.currentUser.Id}&query.startDate=${dateFromFormatted}&query.endDate=${dateToFormatted}`;
        const searchTimesheetsUrl = `${this.apiBaseUrl}/timesheets/search${queryString}`;
        return this.http.get<Timesheet[]>(searchTimesheetsUrl);
    }

    getProjects() {
        const projectsUrl = `${this.apiBaseUrl}/employees/${this.currentUser.Id}/projects`;
        return this.http.get<Project[]>(projectsUrl);
    }

    getTickets() {
        const ticketsUrl = `${this.apiBaseUrl}/tasks/search?taskSearch.responsibleId=${this.currentUser.Id}`;
        return this.http.get<Ticket[]>(ticketsUrl);
    }

    getTicketsByProject(projectId) {
        const ticketsUrl = `${this.apiBaseUrl}/projects/${projectId}/tickets`;
        return this.http.get<Ticket[]>(ticketsUrl);
    }

    createTicket(ticket: Ticket) {
        const url = `${this.apiBaseUrl}/tasks`;
        return this.http.post<Ticket>(url, ticket);
    }

    updateTicket(ticket: Ticket) {
        const url = `${this.apiBaseUrl}/tasks`;
        return this.http.put<Ticket>(url, ticket);
    }

    create(timesheet: Timesheet) {
        const url = `${this.apiBaseUrl}/timesheets`;
        return this.http.post<Timesheet>(url, timesheet);
    }

    update(timesheet: Timesheet) {
        const url = `${this.apiBaseUrl}/timesheets`;
        return this.http.put<Timesheet>(url, timesheet);
    }
}