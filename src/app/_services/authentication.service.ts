import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    login(email: string, password: string) {
        const loginUrl = 'http://aac-vm.universe.dart.spb:8080/api/employees/login';
        return this.http.post<any>(loginUrl, { login: email, password: password })
            .map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.Id) { //HACK: Since we don't control the API and it doesn't return a token, we use the user's Id instead.
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}