import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from "rxjs/observable/forkJoin";
import { Project, Ticket } from '../_models/index';
import { TimesheetService } from '../_services/index';
import { MatDialog } from '@angular/material';
import { NewTicketComponent } from './newTicket/newTicket.component';

@Component({
  moduleId: module.id,
  templateUrl: 'dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  project: Project;
  statuses = [
    { Id: 1, Name: 'Open' },
    { Id: 2, Name: 'Development' },
    { Id: 3, Name: 'Ready for QA' },
    { Id: 4, Name: 'Test' },
    { Id: 5, Name: 'Closed' }
  ];

  constructor(private route: ActivatedRoute, private timesheetService: TimesheetService, public dialog: MatDialog) {
  }

  ngOnInit() {
    const projectId = this.route.snapshot.params.projectId;

    let allFetchMethods = [
      this.timesheetService.getProjects(),
      this.timesheetService.getTicketsByProject(projectId)
    ];

    // We fetch elements in parallel
    forkJoin(allFetchMethods).subscribe(results => {
      const project = results[0].find(x => x.Id == projectId);
      project.Tickets = results[1];
      this.project = project;
    });
  }

  openDialog(project: Project): void {
    this.dialog.open(NewTicketComponent, { data: { project } });
}

  onMove($event: any, targetTicket: Ticket, statusId: number) {
    const ticket = $event.dragData;
    // Since implementing dropZones is too complicated, we just ignore cases where a tikcet is dropped outside its row.
    if (ticket.Id == targetTicket.Id) {
      ticket.StatusId = statusId;
      this.timesheetService.updateTicket(ticket).subscribe();
    }
  }
}
