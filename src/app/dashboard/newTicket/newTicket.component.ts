import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Project, Ticket } from '../../_models/index';
import { TimesheetService } from '../../_services/index';

@Component({
  moduleId: module.id,
  templateUrl: './newTicket.component.html',
  styleUrls: ['./newTicket.component.css']
})
export class NewTicketComponent implements OnInit {
  name: string;
  description: string;
  project: Project;

  constructor(public dialogRef: MatDialogRef<NewTicketComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private timesheetService: TimesheetService) {
    this.project = data.project;
  }

  ngOnInit() {
  }

  onClose() {
    this.dialogRef.close();
  }

  onAdd() {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const ticket = new Ticket(0, this.project.Id, this.name, this.description, currentUser.Id, currentUser.Id);
    this.timesheetService.createTicket(ticket).subscribe(results => {
      this.project.Tickets.push(results);
    });
    this.dialogRef.close();
  }
}
