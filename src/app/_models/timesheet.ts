export class Timesheet {
    constructor(id: number, ticketId: number, loggedTime: number, comment: string, date: string) {
        this.Id = id;
        this.TicketId = ticketId;
        this.LoggedTime = loggedTime;
        this.Comment = comment;
        this.Date = date;
    }
    Id: number;
    TicketId: number;
    LoggedTime: number;
    Comment: string;
    Date: string;
}