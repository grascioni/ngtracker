import { Timesheet } from ".";
import { TimesheetsGroup } from "./timesheetsGroup";

export class Ticket {
    constructor(id: number, projectId: number, name: string, description: string, reporterId: number, responsibleId: number) {
        this.Id = id;
        this.ProjectId = projectId;
        this.Name = name;
        this.Description = description;
        this.ReporterId = reporterId;
        this.ResponsibleId = responsibleId;
        this.StatusId = 1;
        this.TypeId = 1;
    }
    Id: number;
    Name: string;
    Description: string;
    Estimate: number;
    StartDate: Date;
    EndDate: Date;
    TypeId: number;
    StatusId: number;
    ProjectId: number;
    ReporterId: number;
    ResponsibleId: number;
    TimesheetsPerDay: Array<TimesheetsGroup>;
}