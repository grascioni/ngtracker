import { Timesheet } from ".";

export class TimesheetsGroup {

    constructor(day: Date) {
        this.Day = day;
    }

    TotalTime: number;
    Day: Date;
    Timesheets: Array<Timesheet>;

    calculateTotalTime(): void {
        this.TotalTime = this.Timesheets.map(x => x.LoggedTime).reduce((a, b) => a + b, 0);
    }
}