export class DayTotal {
    constructor(day: Date, totalTime: number) {
        this.Day = day;
        this.TotalTime = totalTime;
    }
    TotalTime: number;
    Day: Date;
}