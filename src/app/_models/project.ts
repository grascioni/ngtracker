import { Ticket, TimesheetsGroup, DayTotal } from ".";

export class Project {
    Id: number;
    Name: string;
    Description: string;
    CustomerName: string;
    StartDate: Date;
    EndDate: Date;
    ImageUrl: string;
    Tickets: Array<Ticket>;
    Totals: Array<DayTotal>;
}