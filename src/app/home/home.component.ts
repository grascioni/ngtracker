import { Component, OnInit } from '@angular/core';
import { forkJoin } from "rxjs/observable/forkJoin";
import { Timesheet, Project, Ticket, TimesheetsGroup, DayTotal } from '../_models/index';
import { TimesheetService } from '../_services/index';
import { MatDialog } from '@angular/material';
import * as moment from 'moment';
import { TimesheetsDialogComponent } from './timesheetsDialog/timesheetsDialog.component';

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
    projects: Project[] = [];
    dates: Date[] = [];
    pastWeeks: number = 1;

    constructor(private timesheetService: TimesheetService, public dialog: MatDialog) {
    }

    ngOnInit() {
        this.setDates();
        this.loadAllTimesheets();
    }

    openDialog(project: Project, ticket: Ticket, group: TimesheetsGroup): void {
        this.dialog.open(TimesheetsDialogComponent, { data: { ticket, group } }).afterClosed().subscribe(() => {
            project.Totals = this.getProjectTotalTimes(project.Tickets);
        });
    }

    loadPrevious(): void {
        this.pastWeeks = this.pastWeeks + 1;
        this.setDates();
        this.loadAllTimesheets();
    }

    loadNext(): void {
        this.pastWeeks = this.pastWeeks - 1;
        this.setDates();
        this.loadAllTimesheets();
    }

    setDates(): void {
        this.dates.length = 0;
        var daysCount = 6;
        var startingDay = daysCount * this.pastWeeks;
        var endingDay = startingDay - daysCount;
        for (let index = startingDay; index >= endingDay; index--) {
            var date = new Date();
            date.setDate(date.getDate() - index);
            date.setHours(0, 0, 0, 0);
            this.dates.push(date);
        }
    }

    saveChanges(project: Project, ticket: Ticket, group: TimesheetsGroup, newValue: string) {
        const newNumber = parseInt(newValue);
        // We save changes and update the local state accordingly.
        const timesheets = group.Timesheets.filter(x => this.dateEquals(x, group.Day));
        if (timesheets.length > 0) {
            const existingTimesheet = timesheets[0];
            existingTimesheet.LoggedTime = newNumber;
            // We can recalculate here because the local state is already up to date.
            project.Totals = this.getProjectTotalTimes(project.Tickets);
            // There's no need to implement the subscribe's callback since we already have full information of timesheet.
            this.timesheetService.update(existingTimesheet).subscribe();
        }
        else {
            const dayTotal = project.Totals.find(x => x.Day == group.Day);
            dayTotal.TotalTime = dayTotal.TotalTime + newNumber;
            const stringDate = moment(group.Day).format('MM-DD-YYYY');
            const newTimesheet = new Timesheet(0, ticket.Id, newNumber, null, stringDate);
            this.timesheetService.create(newTimesheet).subscribe(results => {
                group.Timesheets.push(results);
            });
        }
    }

    private getProjectTotalTimes(tickets: Ticket[]): Array<DayTotal> {
        const allGroups = tickets.map(x => x.TimesheetsPerDay).reduce((a, b) => a.concat(b), [])
        return this.dates.map(day => this.getDayTotal(allGroups.filter(x => x.Day == day), day));
    }

    private dateEquals(timesheet: Timesheet, day: Date): boolean {
        const date = moment(timesheet.Date);
        return date.isSame(day, 'day');
    }

    private loadAllTimesheets() {
        let allFetchMethods = [
            this.timesheetService.getProjects(),
            this.timesheetService.getTickets(),
            this.timesheetService.getAll(this.dates[0], this.dates[6])
        ];

        // We fetch elements in parallel
        forkJoin(allFetchMethods).subscribe(results => {
            this.projects = results[0];
            this.projects.forEach(project => {
                project.Tickets = results[1].filter(x => x.ProjectId == project.Id);
                project.Tickets.forEach(ticket => {
                    var ticketTimesheets = results[2].filter(x => x.TicketId == ticket.Id);
                    ticket.TimesheetsPerDay = this.dates.map((day) => this.getTimesheetsGroup(ticketTimesheets, day));
                });
                project.Totals = this.getProjectTotalTimes(project.Tickets);
            });
        });
    }

    private getTimesheetsGroup(timesheets: Array<Timesheet>, day: Date): TimesheetsGroup {
        const group = new TimesheetsGroup(day);
        group.Timesheets = timesheets.filter(x => this.dateEquals(x, day));
        group.calculateTotalTime();
        return group;
    }

    private getDayTotal(dayGroups: Array<TimesheetsGroup>, day: Date): DayTotal {
        //TODO: Refactor to improve clarity.
        const totalTime = dayGroups.map(x => x.Timesheets).reduce((a, b) => a.concat(b), []).map(x => x.LoggedTime).reduce((a, b) => a + b, 0);
        return new DayTotal(day, totalTime);
    }
}