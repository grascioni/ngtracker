import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Timesheet, TimesheetsGroup, Ticket } from '../../_models/index';
import { TimesheetService } from '../../_services/index';
import * as moment from 'moment';

@Component({
  moduleId: module.id,
  templateUrl: 'timesheetsDialog.component.html',
  styleUrls: ['./timesheetsDialog.component.css']
})
export class TimesheetsDialogComponent implements OnInit {
  time: number;
  comment: string;
  ticket: Ticket;
  group: TimesheetsGroup;


  constructor(public dialogRef: MatDialogRef<TimesheetsDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private timesheetService: TimesheetService) {
    this.ticket = data.ticket;
    this.group = data.group;
  }

  ngOnInit() { }

  onClose() {
    this.dialogRef.close();
  }
  onAdd() {
    const stringDate = moment(this.data.group.Day).format('MM-DD-YYYY');
    const timesheet = new Timesheet(0, this.data.ticket.Id, this.time, this.comment, stringDate);
    this.timesheetService.create(timesheet).subscribe(results => {
      this.group.Timesheets.push(results);
      this.group.calculateTotalTime();
    });
  }
}
